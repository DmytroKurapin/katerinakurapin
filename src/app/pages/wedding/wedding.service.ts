import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { ContentItem } from '@shared/helpers/content-item';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class WeddingService {

	constructor(private http: HttpClient) {
	}

	public getContent(): Observable<ContentItem[]> {
		return this.http.get<ContentItem[]>(`${environment.serverUrl}api/content/wedding`).pipe(take(1));
	}
}
