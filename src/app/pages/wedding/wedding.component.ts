import { Component, OnInit } from '@angular/core';
import { ContentItem } from '@shared/helpers/content-item';
import { Observable } from 'rxjs';
import { WeddingService } from './wedding.service';

@Component({
	selector: 'wedding',
	templateUrl: './wedding.component.html',
	styleUrls: ['./wedding.component.scss'],
	providers: [WeddingService]

})
export class WeddingComponent implements OnInit {
	contentArr$: Observable<ContentItem[]>;

	constructor(private wedService: WeddingService) {
		this.contentArr$ = this.wedService.getContent();
	}

	ngOnInit() {
	}

}
