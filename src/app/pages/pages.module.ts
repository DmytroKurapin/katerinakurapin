import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { BarMitzvahComponent } from './bar-mitzvah/bar-mitzvah.component';
import { BatMitzvahComponent } from './bat-mitzvah/bat-mitzvah.component';
import { ContactsComponent } from './contacts/contacts.component';
import { GreetingCardsComponent } from './greeting-cards/greeting-cards.component';
import { WeddingComponent } from './wedding/wedding.component';

@NgModule({
	declarations: [WeddingComponent, BarMitzvahComponent, BatMitzvahComponent, ContactsComponent, GreetingCardsComponent, ],
	imports: [
		CommonModule,
		SharedModule,
		RouterModule.forChild([
			{ path: 'wedding', component: WeddingComponent },
			{ path: 'bar-mitzvah', component: BarMitzvahComponent },
			{ path: 'bat-mitzvah', component: BatMitzvahComponent },
			{ path: 'greeting-cards', component: BatMitzvahComponent },
			{ path: 'contacts', component: BatMitzvahComponent },
			{ path: '**', redirectTo: 'wedding' }
		])
	],
})
export class PagesModule {
}
