import { Component, OnDestroy, OnInit } from '@angular/core';
import { DeviceTypeService } from '@services/device-type.service';
import { DirectionService } from '@services/direction.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
	private currLangCode: string;
	private dirSubscription: Subscription;

	constructor(private dirService: DirectionService, public deviceService: DeviceTypeService) {
	}

	ngOnInit() {
		this.dirSubscription = this.dirService.currDir$.subscribe(obj => this.currLangCode = obj.code === 'en' ? '' : obj.code);
	}

	ngOnDestroy(): void {
		this.dirSubscription.unsubscribe();
	}

	public routeLang(route: string): string[] {
		return [`/${this.currLangCode}${route}`];
	}

}
