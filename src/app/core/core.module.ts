import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { AppRoutingModule } from '../app-routing.component';
import { HomeComponent } from '../pages/home/home.component';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
	declarations: [HomeComponent, HeaderComponent, NavbarComponent],
	imports: [
		CommonModule,
		FormsModule,
		SharedModule,
		AppRoutingModule
	],
	exports: [NavbarComponent, HeaderComponent]
})
export class CoreModule {
}
