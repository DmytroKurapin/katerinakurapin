import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { DeviceTypeService } from '@services/device-type.service';
import { DirectionService } from '@services/direction.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
	public searchText = '';
	public selectedLang: string;
	private dirServiceSubscr: Subscription;

	@Output() openSidenav: EventEmitter<any>;

	constructor(public dirService: DirectionService, public deviceService: DeviceTypeService) {
		this.openSidenav = new EventEmitter<any>();
	}

	ngOnInit() {
		this.dirServiceSubscr = this.dirService.currDir$.subscribe(dirObj => this.selectedLang = dirObj.code);
	}

	ngOnDestroy(): void {
		this.dirServiceSubscr.unsubscribe();
	}

	onChangeLang(code: string) {
		this.dirService.updateLanguage(code);
	}

	public onResize(ev) {
	}

}
