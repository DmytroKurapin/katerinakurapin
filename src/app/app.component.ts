import { Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { Router } from '@angular/router';
import { DeviceTypeService } from '@services/device-type.service';
import { DirectionService } from '@services/direction.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	@ViewChild('sidenav') sidenav: MatSidenav;
	public title = 'katerinakurapin';

	constructor(private _router: Router, public dirService: DirectionService, public deviceService: DeviceTypeService) {
		// todo save each opened item in db for statistics
	}
}
