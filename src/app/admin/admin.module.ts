import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './services/auth.guard';
import { AuthService } from './services/auth.service';

@NgModule({
	declarations: [LoginComponent, DashboardComponent],
	imports: [
		CommonModule,
		SharedModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forChild([
			{ path: '', component: LoginComponent },
			{ path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
			{ path: '**', redirectTo: '' }
		])
	],
	providers: [AuthGuard, AuthService]
})
export class AdminModule {
}
