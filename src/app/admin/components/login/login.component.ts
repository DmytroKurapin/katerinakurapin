import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
	selector: 'login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	loginForm: FormGroup;
	loginError: string;

	constructor(public fb: FormBuilder, private auth: AuthService, private router: Router) {
		// todo save each incorrect try in db for statistics
		if (this.auth.isAuthorized) {
			this.router.navigate(['/admin/dashboard']);
		}
	}

	ngOnInit() {
		this.loginForm = this.fb.group({
			email: ['', Validators.compose([Validators.required, Validators.email])],
			password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
		});
	}

	async login() {
		this.loginError = null;
		const { email, password } = this.loginForm.value;
		if (!email) {
			return;
		}
		const credentials = { email, password };
		this.auth.signIn(credentials)
			.subscribe(async resp => {
				const { error, token } = resp;
				if (error) {
					this.loginError = error;
					return;
				}
				if (!token) {
					this.loginError = 'Incorrect Email Or Password';
					return;
				}
				this.auth.saveToken(token);
				this.enterToAdminDashboard();
				return;
			}, e => {
				this.loginError = e;
			});
	}

	enterToAdminDashboard() {
		this.loginForm.reset();
		return this.router.navigate(['/admin/dashboard']);
	}

}
