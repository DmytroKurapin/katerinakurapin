import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable()
export class AuthService {

	constructor(private http: HttpClient) { }

	get isAuthorized(): boolean {
		return !!localStorage.getItem('token');
	}

	signIn(credentials: { email: string, password: string }): Observable<any> {
		return this.http.post(`${environment.serverUrl}api/auth/login`, credentials).pipe(take(1));
	}

	saveToken(token: string) {
		// todo replace localStorage on something more secure
		localStorage.setItem('token', token);
	}
}
