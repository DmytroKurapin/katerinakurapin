import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'il', component: HomeComponent, pathMatch: 'full' },
	{ path: 'ru', component: HomeComponent, pathMatch: 'full' },
	{	path: 'pages', loadChildren: () => import('./pages/pages.module').then(mod => mod.PagesModule) },
	{	path: 'il/pages', loadChildren: () => import('./pages/pages.module').then(mod => mod.PagesModule) },
	{	path: 'ru/pages', loadChildren: () => import('./pages/pages.module').then(mod => mod.PagesModule) },
	{	path: 'admin', loadChildren: () => import('./admin/admin.module').then(mod => mod.AdminModule) },
	{	path: 'il/admin', loadChildren: () => import('./admin/admin.module').then(mod => mod.AdminModule) },
	{	path: 'ru/admin', loadChildren: () => import('./admin/admin.module').then(mod => mod.AdminModule) },

	{ path: '**', redirectTo: '' }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
