import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DeviceTypeService } from '@services/device-type.service';
import { DirectionService } from '@services/direction.service';
import { ContentItem } from '@shared/helpers/content-item';
import { Observable } from 'rxjs';
import { ChosenItemComponent } from '../chosen-item/chosen-item.component';

@Component({
	selector: 'tile-template',
	templateUrl: './tile-template.component.html',
	styleUrls: ['./tile-template.component.scss']
})
export class TileTemplateComponent implements OnInit {
	public colsNum: number;
	public gutterSize: string;
	public visibleImages: number[] = [];
	private mdScreenSize = 980;
	private maxScreenSize = 1080;
	private contentWidth = 1320;
	private readonly coefScreen: number;
	private minGutterRem = 1;
	private maxGutterRem = 2.3;

	@Input() content: Observable<ContentItem[]>;

	constructor(private dialog: MatDialog, public dirService: DirectionService, public deviceService: DeviceTypeService) {
		this.coefScreen = (this.maxGutterRem - this.minGutterRem) / (this.maxScreenSize - this.mdScreenSize);
	}

	ngOnInit() {
		this.setGridAttrs(window.innerWidth);
	}

	public onResize(event) {
		this.setGridAttrs(event.target.innerWidth);
	}

	public openChoseItem(obj: ContentItem) {
		const dialogRef = this.dialog.open(ChosenItemComponent, {
			data: obj,
			height: '90vh',
			width: '90vw',
			autoFocus: false,
			direction: this.dirService.currDir$.value.dir
		});

		dialogRef.afterClosed().subscribe(result => {
			console.log(`Dialog result: ${result}`);
		});
	}

	public trackByFn(index, item) {
		return item.articul;
	}

	public imgLoaded(index: number) {
		this.visibleImages.push(index);
	}

	private setGridAttrs(innerWidth: number) {
		switch (true) {
			case innerWidth <= this.mdScreenSize:
				this.gutterSize = `${this.minGutterRem}rem`;
				this.colsNum = 2;
				break;
			case innerWidth <= this.maxScreenSize:
				this.gutterSize = `${this.minGutterRem + (innerWidth - this.mdScreenSize) * this.coefScreen}rem`;
				this.colsNum = 2;
				break;
			case innerWidth <= this.contentWidth:
				this.gutterSize = `${this.minGutterRem + (innerWidth - this.maxScreenSize) * this.coefScreen}rem`;
				this.colsNum = 3;
				break;
			default:
				this.gutterSize = '2rem';
				this.colsNum = 3;
				break;
		}
	}

}
