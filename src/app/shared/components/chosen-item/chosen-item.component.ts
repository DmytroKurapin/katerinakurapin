import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ContentItem } from '@shared/helpers/content-item';

@Component({
	selector: 'chosen-item',
	templateUrl: './chosen-item.component.html',
	styleUrls: ['./chosen-item.component.scss']
})
export class ChosenItemComponent implements OnInit {
	public activeImgIdx = 0;

	constructor(@Inject(MAT_DIALOG_DATA) public data: ContentItem) {
	}

	ngOnInit() {
	}

	public choseImg(idx: number) {
		this.activeImgIdx = idx;
	}
}
