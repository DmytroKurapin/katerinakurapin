import { Injectable } from '@angular/core';
import { Event, NavigationEnd, Router } from '@angular/router';
import { LanguageObject } from '@shared/helpers/language-object';
import { BehaviorSubject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class DirectionService {
	get langArr(): LanguageObject[] {
		return this._langArr;
	}

	get currDir$(): BehaviorSubject<LanguageObject> {
		return this._currDir;
	}

	private readonly _langArr: LanguageObject[] = [
		{ code: 'en', dir: 'ltr', text: 'English' },  // default lang
		{ code: 'ru', dir: 'ltr', text: 'Русский' },
		{ code: 'il', dir: 'rtl', text: 'עברית' },
	];
	private readonly _currDir: BehaviorSubject<LanguageObject>;

	constructor(private router: Router) {
		this._currDir = new BehaviorSubject(this._langArr[0]);
		this.router.events.subscribe((ev: Event) => {
			// event instanceof NavigationStart - Show loading indicator
			// event instanceof NavigationEnd - Hide loading indicator
			// event instanceof NavigationError ( console.log(event.error) ) - Hide loading indicator

			if (ev instanceof NavigationEnd) {
				const { langArr, currDir$ } = this;
				const currLangObj = langArr.find(obj => ev.url.startsWith(`/${obj.code}/`) || ev.url === `/${obj.code}`) || langArr[0];
				if (!currLangObj || currLangObj.dir === currDir$.value.dir) {
					return;
				}
				currDir$.next(currLangObj);
				return;
			}
		});
	}

	public updateLanguage(code: string) {
		const newLangObj = this.langArr.find(obj => obj.code === code) || this.langArr[0];
		if (this.currDir$.value.code === 'en') {
			// default lang
			this.router.navigate([`/${code}${this.router.url}`]);
		} else {
			this.langArr.find(obj => {
				const re = new RegExp(`^(\\/${obj.code}$)|^(\\/${obj.code})\\/`);
				const isMatched = re.test(this.router.url);
				if (isMatched) {
					const newRoute = this.router.url.replace(`/${obj.code}`, code === 'en' ? '' : code);
					this.router.navigate([newRoute]);
				}
				return isMatched;
			});
		}
		this.currDir$.next(newLangObj);
	}
}
