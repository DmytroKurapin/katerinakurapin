import { Platform } from '@angular/cdk/platform';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class DeviceTypeService {
	private readonly _isMobileScreen: BehaviorSubject<boolean>;
	private readonly _currScreenWidth: BehaviorSubject<number>;
	private readonly _currScreenHeight: BehaviorSubject<number>;
	private mobileScreenBorder = 850;

	constructor(public platform: Platform) {
		this._isMobileScreen = new BehaviorSubject(window.innerWidth < this.mobileScreenBorder);
		this._currScreenWidth = new BehaviorSubject(window.innerWidth);
		this._currScreenHeight = new BehaviorSubject(window.innerHeight);
	}

	get isMobileScreen$(): BehaviorSubject<boolean> {
		return this._isMobileScreen;
	}

	get currScreenWidth$(): BehaviorSubject<number> {
		return this._currScreenWidth;
	}

	get currScreenHeight$(): BehaviorSubject<number> {
		return this._currScreenHeight;
	}

	get isIOS(): boolean {
		return this.platform.IOS;
	}

	get isAndroid(): boolean {
		return this.platform.ANDROID;
	}

	get isMobileDevice(): boolean {
		return this.isIOS || this.isAndroid;
	}

	setScreenParams({ innerWidth, innerHeight }): void {
		this.currScreenWidth$.next(innerWidth);
		this.currScreenHeight$.next(innerHeight);

		if (innerWidth < this.mobileScreenBorder && !this.isMobileScreen$.value) {
			return this.isMobileScreen$.next(true);
		}
		if (innerWidth > this.mobileScreenBorder && this.isMobileScreen$.value) {
			return this.isMobileScreen$.next(false);
		}
	}
}
