import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DeviceTypeService } from '@services/device-type.service';
import { DirectionService } from '@services/direction.service';
import { ChosenItemComponent } from '@shared/components/chosen-item/chosen-item.component';
import { TileTemplateComponent } from '@shared/components/tile-template/tile-template.component';
import { CustomMaterialModule } from './custom-material-module/custom-material.module';
import { LazyLoadingImgDirective } from './directives/lazy-loading-img.directive';

const importedComps = [TileTemplateComponent, ChosenItemComponent, LazyLoadingImgDirective];

@NgModule({
	declarations: importedComps,
	imports: [
		CommonModule,
		CustomMaterialModule,
	],
	exports: [CustomMaterialModule, ...importedComps],
	entryComponents: [ChosenItemComponent],
	providers: [DirectionService, DeviceTypeService]
})
export class SharedModule {
}
