import { AfterViewInit, Directive, ElementRef, HostBinding, Input } from '@angular/core';

@Directive({
	selector: '[lazyLoadingImg]'
})
export class LazyLoadingImgDirective implements AfterViewInit {
	@HostBinding('attr.src') srcAttr = null;
	@Input() src: string;
	private readonly nativeElement: HTMLElement;
	private parentEl: Element;

	constructor(private el: ElementRef) {
		this.nativeElement = this.el.nativeElement;
	}

	ngAfterViewInit() {
		this.parentEl = this.nativeElement.closest('mat-grid-tile');
		this.canLazyLoad() ? this.lazyLoadImage() : this.loadImage();
	}

	private canLazyLoad() {
		return window && 'IntersectionObserver' in window;
	}

	private lazyLoadImage() {
		const obs = new IntersectionObserver(entries => {
			entries.forEach(a => {
				if (a.isIntersecting) {
					this.loadImage();
					obs.unobserve(this.parentEl);
				}
			});
		});
		obs.observe(this.parentEl);
	}

	private loadImage() {
		this.srcAttr = this.src;
	}
}
