export interface SpecialOffer {
	price: number;
	isNew: boolean;
}
