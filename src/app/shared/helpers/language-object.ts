import { Direction } from '@angular/cdk/bidi';

export interface LanguageObject {
	code: string;
	dir: Direction;
	text: string;
}
