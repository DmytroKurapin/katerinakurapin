import { SpecialOffer } from '@shared/helpers/special-offer';

export interface ContentItem {
	title: string;
	description: string;
	page: string;
	images: string[];
	price: number;
	thumbnail: string;
	order: number;
	specialOffer: SpecialOffer;
	articul: string;
}
