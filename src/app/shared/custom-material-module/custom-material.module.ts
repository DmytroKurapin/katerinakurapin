import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatDialogModule, MatInputModule, MatSelectModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';

const matModules = [
	CommonModule,
	MatMenuModule,
	MatIconModule,
	MatInputModule,
	MatSelectModule,
	MatButtonModule,
	MatDialogModule,
	MatDividerModule,
	MatGridListModule,
];

@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		...matModules,
	],
	exports: [...matModules]
})
export class CustomMaterialModule {
}
